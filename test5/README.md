# 实验5：包，过程，函数的用法
**姓名：刘宏；**
**学号：202010414410**
## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![](step1.png)

*这段代码是创建一个包(PACKAGE)名为MyPack，包含一个返回部门总薪资的函数Get_SalaryAmount和一个递归显示员工信息的过程Get_Employees，包体(PACKAGE BODY)中实现了这两个子程序的具体实现。*

*函数Get_SalaryAmount根据传入的部门ID(V_DEPARTMENT_ID)，查询员工表中该部门的所有员工薪资总和并返回结果。*

*过程Get_Employees根据传入的员工ID(V_EMPLOYEE_ID)，通过递归查询，逐级查找员工的上级领导信息，并以树形结构输出员工的信息。*

*注意事项：在包中可以声明全局变量，供包体中的多个子程序使用，但要注意控制变量的作用域和声明的类型。此外，在包体中使用DBMS_OUTPUT.PUT_LINE可以在SQL Developer或PL/SQL Developer中的输出窗口显示调试信息。*


## 测试

```sql
函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

```
![](step2.png)

*这是一个SQL查询语句，查询departments表中的department_id、department_name和调用包MyPack中的Get_SalaryAmount函数获取每个部门的薪资总和，将结果重命名为salary_total。*

*该语句的目的是显示每个部门的薪资总和，从而更好地了解公司的财务状况。查询结果将包含三列数据，分别是department_id、department_name和salary_total。其中salary_total是通过Get_SalaryAmount函数计算得到的。*

```sql
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```
![](step3.png)
*上述运行代码是一个PL/SQL代码块，主要作用是通过调用包MyPack中的Get_Employees过程显示员工信息。*

*代码块的第一行设置了服务器输出(serveroutput)参数，以便在SQL Developer或PL/SQL Developer的输出窗口中显示调试信息。*

*接下来，使用DECLARE关键字声明要使用的变量V_EMPLOYEE_ID，其数据类型为NUMBER。*

*在BEGIN...END语句块中，将V_EMPLOYEE_ID的初始值设置为101，然后调用包MyPack中的Get_Employees过程，并将V_EMPLOYEE_ID作为参数传递给这个过程。*

*执行完毕后，将显示基于Get_Employees过程的递归查询结果，结果显示在SQL Developer或PL/SQL Developer的输出窗口中。*

- 使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工:

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
![](step4.png)

*上面的代码是一个SQL查询语句，查询员工表中的LEVEL、EMPLOYEE_ID、FIRST_NAME和MANAGER_ID字段。它使用了Oracle的递归查询功能，通过START WITH和CONNECT BY子句实现从起始点向下递归查询的功能，查询结果以树形结构展示员工和其上级领导的关系。*

*具体而言，通过START WITH子句指定查询的起始点，即EMPLOYEE_ID等于V_EMPLOYEE_ID的员工；通过CONNECT BY子句指定查询的递归条件，即每个员工的EMPLOYEE_ID与其上级领导的MANAGER_ID相连通。*

*查询结果包括四列数据：LEVEL表示递归的级别，而EMPLOYEE_ID、FIRST_NAME和MANAGER_ID分别代表员工编号、姓名和直接上级的编号。*


## 实验总结
1. 本次实验的主要目的是让学生能够熟练掌握PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程和函数的用法。

2. 在本次实验中，我们使用Oracle数据库和SQL Developer工具进行编码和测试。实验包含了创建包和其内部的函数和过程，同时使用游标、递归查询等功能，实现了查询每个部门的工资总额和树形结构输出员工信息的功能。

3. 通过本次实验，我们能够了解如何使用PL/SQL来实现复杂的数据处理功能，并且加深对Oracle数据库的理解和掌握。

4. 在实验过程中，我们需要注意变量的作用域和声明的类型，正确使用PL/SQL语言结构和关键字，并且保持良好的编码风格和正确的注释。

5. 总之，本次实验是一次结合理论和实践的综合性实验，让我们全面掌握PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程和函数的用法，提高了我们对Oracle数据库的应用水平和编程能力。