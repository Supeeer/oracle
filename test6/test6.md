﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计
**姓名：刘宏**

**学号：202010414410**

**班级：20级软工四班**

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验内容
确定实验大致步骤：

步骤一：权限及用户分配方案

    创建至少两个用户，用于不同层级的权限管理。
    为每个用户分配适当的权限，以确保系统的安全性和数据的保护。

步骤二：数据库设计方案

    确定所需的表及其结构，以满足商品销售系统的需求。至少需要设计4张表，并确保总模拟数据量不少于10万条。
    设计表之间的关系和约束，如主键、外键和唯一性约束等。
    创建适当的索引来提高查询性能。
    划分表空间，至少创建两个表空间，用于存储不同类型的数据。

步骤三：PL/SQL存储过程和函数设计

    在数据库中创建一个程序包，用于存储存储过程和函数。
    使用PL/SQL语言设计一些复杂的业务逻辑的存储过程和函数，以满足商品销售系统的需求。例如，可以创建存储过程来处理订单、库存管理或报表生成等功能。

步骤四：数据库备份方案

    设计一套数据库的备份方案，以确保数据的安全性和可恢复性。
    确定备份的频率和方式，如完全备份、增量备份或差异备份。
    创建适当的备份策略，包括定期备份和日志备份。
    
1. 创建一个新的用户sale_lh，同时给他授权

创建用户和分配权限的SQL代码示例：
```sql
-- 创建用户
CREATE USER sale_lh IDENTIFIED BY 123;
-- 授予用户权限
GRANT CONNECT, RESOURCE TO sales_user;
```
![](1.png)
![](2.png)
2. 建立一个新的连接（即数据库），名为：pdborcl_lh

![](3.png)

3. 创建出两个表空间，便于后续将表存入表空间。同时将表空间分配给用户sale_lh。在Oracle数据库中，表空间（tablespace）是用来存储数据的逻辑容器，而表（table）则是存储实际数据的数据库对象。通过合理设计和管理表空间，可以提高数据库的性能、灵活性和可管理性。表空间的创建、管理和分配可以通过Oracle的管理工具或使用SQL语句进行操作。
```sql
--创建表空间
CREATE TABLESPACE sale_tablespace
  DATAFILE 'sale_tablespace.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE UNLIMITED;

CREATE TABLESPACE temp_tablespace
  DATAFILE 'temp_tablespace.dbf'
  SIZE 50M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE UNLIMITED;
-- 分配表空间给用户
ALTER USER sale_lh DEFAULT TABLESPACE sales_tablespace;
ALTER USER sale_lh DEFAULT TABLESPACE temp_tablespace;
```
![](4.png)
![](6.png)

4. 在新的数据库中创建4张数据表：customer(用户表)、ordes(订单表)、order_details(订单数据表)、products(商品表)
```sql
-- 创建商品表并指定表空间
CREATE TABLE products (
  product_id   NUMBER PRIMARY KEY,
  product_name VARCHAR2(100) NOT NULL,
  price        NUMBER(10, 2) NOT NULL,
  quantity     NUMBER
)
TABLESPACE sales_tablespace;

-- 创建订单表并指定表空间
CREATE TABLE orders (
  order_id    NUMBER PRIMARY KEY,
  order_date  DATE NOT NULL,
  customer_id NUMBER REFERENCES customers(customer_id)
)
TABLESPACE sales_tablespace;

-- 创建订单明细表并指定表空间
CREATE TABLE order_details (
  order_id   NUMBER REFERENCES orders(order_id),
  product_id NUMBER REFERENCES products(product_id),
  quantity   NUMBER,
  price      NUMBER(10, 2),
  PRIMARY KEY (order_id, product_id)
)
TABLESPACE sales_tablespace;

-- 创建用户表并指定表空间
CREATE TABLE customers (
  customer_id   NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100) NOT NULL,
  email         VARCHAR2(100)
)
TABLESPACE sales_tablespace;
```
* products 表包含商品的基本信息：product_id、product_name、price 和 quantity。其中 product_id 是主键。
* orders 表包含订单的基本信息：order_id、order_date 和 customer_id。其中 order_id 是主键，customer_id 是外键，参考 customers 表的主键 customer_id。
* order_details 表包含订单明细的信息：order_id、product_id、quantity 和 price。其中 order_id 和 product_id 组成主键，同时是外键，分别参考 orders 表和 products 表的主键。
* customers 表包含客户的基本信息：customer_id、customer_name 和 email。其中 customer_id 是主键。

![](5.png)

5. 授予用户操作表格的权限
```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_lh;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sale_lh;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sale_lh;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sale_lh;
```
![](7.png)

6. 新创建的表格仍为空表，因此我们需要插入数据。我选择的是在四张表中均插入3w条数据，累计12w的虚拟数据。插入数据之前出现了错误，原因是我在授权表空间的时候语句并没有生效，所以我使用了新的语句，并成功运行了。
```sql
-- 授予表空间权限给 sale_lh 用户
ALTER USER sale_lh QUOTA UNLIMITED ON sales_tablespace;
ALTER USER sale_lh QUOTA UNLIMITED ON temp_tablespace;
```
![](9.png)
```sql
-- 插入虚拟数据到products表
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO products p
    USING (
      SELECT i AS product_id, 'Product ' || i AS product_name, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
      FROM dual
    ) data
    ON (p.product_id = data.product_id)
    WHEN NOT MATCHED THEN
      INSERT (product_id, product_name, price, quantity)
      VALUES (data.product_id, data.product_name, data.price, data.quantity);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到orders表（假设customers表已经有了对应的数据）
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO orders o
    USING (
      SELECT i AS order_id, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS order_date, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customer_id
      FROM dual
    ) data
    ON (o.order_id = data.order_id)
    WHEN NOT MATCHED THEN
      INSERT (order_id, order_date, customer_id)
      VALUES (data.order_id, data.order_date, data.customer_id);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到order_details表（假设products表已经有了对应的数据）
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO order_details od
    USING (
      SELECT i AS order_id, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS product_id, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
      FROM dual
    ) data
    ON (od.order_id = data.order_id AND od.product_id = data.product_id)
    WHEN NOT MATCHED THEN
      INSERT (order_id, product_id, quantity, price)
      VALUES (data.order_id, data.product_id, data.quantity, data.price);
  END LOOP;
  COMMIT;
END;
/

-- 插入虚拟数据到customers表
BEGIN
  FOR i IN 1..30000 LOOP
    MERGE INTO customers c
    USING (
      SELECT i AS customer_id, 'Customer ' || i AS customer_name, 'customer' || i || '@example.com' AS email
      FROM dual
    ) data
    ON (c.customer_id = data.customer_id)
    WHEN NOT MATCHED THEN
      INSERT (customer_id, customer_name, email)
      VALUES (data.customer_id, data.customer_name, data.email);
  END LOOP;
  COMMIT;
END;
/
```
* 第一个代码块向 products 表插入 30000 条虚拟数据，包括 product_id、product_name、price 和 quantity 四个字段。其中 product_id 从 1 开始，每次递增 1；product_name 格式为 "Product xxx"，其中 xxx 是 product_id 的值；price 是从 10 到 100 之间的一个随机小数，保留 2 位小数；quantity 是从 1 到 100 之间的一个随机整数。
* 第二个代码块向 orders 表插入 30000 条虚拟数据，包括 order_id、order_date 和 customer_id 三个字段。其中 order_id 从 1 开始，每次递增 1；order_date 是当前日期减去一个随机数（1 到 365 之间的整数）得到的日期；customer_id 是从 1 到 1000 之间的一个随机整数。
* 第三个代码块向 order_details 表插入 30000 条虚拟数据，包括 order_id、product_id、quantity 和 price 四个字段。其中 order_id 和 product_id 分别从 1 到 30000 之间的一个随机整数；quantity 是从 1 到 10 之间的一个随机整数；price 是从 10 到 100 之间的一个随机小数，保留 2 位小数。注意，这里的 order_id 和 product_id 是组合主键。
* 第四个代码块向 customers 表插入 30000 条虚拟数据，包括 customer_id、customer_name 和 email 三个字段。其中 customer_id 从 1 开始，每次递增 1；customer_name 格式为 "Customer xxx"，其中 xxx 是 customer_id 的值；email 格式为 "customerxxx@example.com"，其中 xxx 是 customer_id 的值。

![](8.png)
    
7. 创建出第一个程序包，名为 sales_pkg 的程序包(PACKAGE)和其体(PACKAGE BODY)包含了两个子程序。
```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg IS

  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN orders.order_id%TYPE,
    p_total_amount OUT NUMBER
  );

  -- 函数：获取客户的订单数量
  FUNCTION get_customer_order_count(
    p_customer_id IN customers.customer_id%TYPE
  ) RETURN NUMBER;

END sales_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_pkg IS

  -- 存储过程：计算订单总金额
  PROCEDURE calculate_order_total(
    p_order_id IN orders.order_id%TYPE,
    p_total_amount OUT NUMBER
  ) IS
    v_total_amount NUMBER;
  BEGIN
    SELECT SUM(od.quantity * od.price)
    INTO v_total_amount
    FROM order_details od
    WHERE od.order_id = p_order_id;

    p_total_amount := v_total_amount;
  END calculate_order_total;

  -- 函数：获取客户的订单数量
  FUNCTION get_customer_order_count(
    p_customer_id IN customers.customer_id%TYPE
  ) RETURN NUMBER IS
    v_order_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_order_count
    FROM orders
    WHERE customer_id = p_customer_id;

    RETURN v_order_count;
  END get_customer_order_count;

END sales_pkg;
/
```
* calculate_order_total 是一个存储过程，接收一个订单号参数 p_order_id，通过查询 order_details 表计算该订单的总金额，并将结果输出到 p_total_amount 参数中。
* get_customer_order_count 是一个函数，接收一个客户号参数 p_customer_id，通过查询 orders 表返回该客户的订单数量。
![](10.png)

为验证程序包的可执行性。下面是程序包的简单调用示例：
```sql
-- 示例调用存储过程：计算订单总金额
DECLARE
  v_order_id orders.order_id%TYPE := 123; -- 替换为实际的订单ID
  v_total_amount NUMBER;
BEGIN
  -- 调用存储过程
  sales_pkg.calculate_order_total(v_order_id, v_total_amount);

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('订单总金额：' || v_total_amount);
END;
/

-- 示例调用函数：获取客户的订单数量
DECLARE
  v_customer_id customers.customer_id%TYPE := 456; -- 替换为实际的客户ID
  v_order_count NUMBER;
BEGIN
  -- 调用函数
  v_order_count := sales_pkg.get_customer_order_count(v_customer_id);

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('客户的订单数量：' || v_order_count);
END;
/
```
![](11.png)

8. 创建出第二个程序包，名为 sales_pkg 的程序包，包含了两个子程序：get_least_sold_product 和 get_max_purchase_amount。
```sql
-- 创建程序包规范
CREATE OR REPLACE PACKAGE sales_pkg IS

  -- 存储过程：获取订单销售量最少的产品
  PROCEDURE get_least_sold_product(
    p_product_id OUT products.product_id%TYPE,
    product_name OUT products.product_name%TYPE,
    p_quantity_sold OUT NUMBER
  );

  -- 函数：计算购买金额最大的用户的购买额
  FUNCTION get_max_purchase_amount RETURN NUMBER;

END sales_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_pkg IS

  -- 存储过程：获取订单销售量最少的产品
  PROCEDURE get_least_sold_product(
    p_product_id OUT products.product_id%TYPE,
    product_name OUT products.product_name%TYPE,
    p_quantity_sold OUT NUMBER
  ) IS
  BEGIN
    SELECT product_id, product_name, total_quantity_sold
    INTO p_product_id, product_name, p_quantity_sold
    FROM (
      SELECT product_id, product_name, SUM(quantity) AS total_quantity_sold
      FROM order_details
      GROUP BY product_id, product_name
      ORDER BY total_quantity_sold ASC
    )
    WHERE ROWNUM = 1;
  END get_least_sold_product;

  -- 函数：计算购买金额最大的用户的购买额
  FUNCTION get_max_purchase_amount RETURN NUMBER IS
    v_max_purchase_amount NUMBER;
  BEGIN
    SELECT MAX(total_amount)
    INTO v_max_purchase_amount
    FROM (
      SELECT c.customer_id, c.customer_name, SUM(od.quantity * od.price) AS total_amount
      FROM customers c
      JOIN orders o ON c.customer_id = o.customer_id
      JOIN order_details od ON o.order_id = od.order_id
      GROUP BY c.customer_id, c.customer_name
    );

    RETURN v_max_purchase_amount;
  END get_max_purchase_amount;

END sales_pkg;
/
```
* get_least_sold_product 存储过程用于查询所有产品（products 表）的销售量，找到销售量最少的产品，同时返回该产品的 product_id、product_name 和销售量 p_quantity_sold。
* get_max_purchase_amount 函数用于计算所有客户（customers 表）的购买金额，找到购买金额最大的客户，并返回其购买额。
![](12.png)

为验证程序包的可执行性。下面是程序包的简单调用示例：
```sql
DECLARE
  v_product_id products.product_id%TYPE;
  v_product_name products.product_name%TYPE;
  v_quantity_sold NUMBER;
  v_max_purchase_amount NUMBER;
BEGIN
  -- 调用存储过程 get_least_sold_product
  sales_pkg2.get_least_sold_product(v_product_id, v_product_name, v_quantity_sold);

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('最少销售量的产品信息：');
  DBMS_OUTPUT.PUT_LINE('Product ID: ' || v_product_id);
  DBMS_OUTPUT.PUT_LINE('Product Name: ' || v_product_name);
  DBMS_OUTPUT.PUT_LINE('Quantity Sold: ' || v_quantity_sold);

  -- 调用函数 get_max_purchase_amount
  v_max_purchase_amount := sales_pkg2.get_max_purchase_amount;

  -- 打印结果
  DBMS_OUTPUT.PUT_LINE('购买金额最大的用户的购买额：' || v_max_purchase_amount);
END;
```
![](13.png)

9. 创建第二个用户
```sql
CREATE USER sale_user IDENTIFIED BY 111;
```
![](14.png)

10. 在开始备份数据库前，先初始化进行数据库的初始化
```sql
-- 使用 SQL*Plus 登录到数据库
sqlplus / as sysdba

-- 关闭数据库
SHUTDOWN IMMEDIATE

-- 启动数据库实例并挂载数据库
STARTUP MOUNT

-- 将数据库切换到归档日志模式
ALTER DATABASE ARCHIVELOG；

-- 打开数据库
ALTER DATABASE OPEN；
```
![](15.png)
```sql
-- 使用 RMAN 工具进行备份
rman target /

-- 显示 RMAN 的当前配置和参数设置
SHOW ALL;

-- 执行完整数据库备份操作
BACKUP DATABASE;

-- 列出已备份的备份集信息
LIST BACKUP;
```
![](16.png)
![](17.png)

11. 第二备份方案。下面是一个使用 RMAN（Recovery Manager）工具进行完整备份的 Oracle 数据库备份脚本示例：
```sql
-- 创建一个 RMAN 脚本文件（backup.rman）

-- 设置备份配置参数
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup/%F';
CONFIGURE DEVICE TYPE DISK BACKUP TYPE TO COMPRESSED BACKUPSET;
CONFIGURE RETENTION POLICY TO REDUNDANCY 2;

-- 开始备份
RUN {
  -- 设置备份路径和标签
  ALLOCATE CHANNEL c1 DEVICE TYPE DISK FORMAT '/backup/full_%U';
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG TAG 'FULL_BACKUP';
  
  -- 备份完成后标记为已备份
  SQL 'ALTER SYSTEM ARCHIVE LOG CURRENT';
  DELETE NOPROMPT OBSOLETE;
  CROSSCHECK BACKUP;
  CROSSCHECK ARCHIVELOG ALL;
  DELETE NOPROMPT EXPIRED BACKUP;
  
  -- 释放备份通道
  RELEASE CHANNEL c1;
}
```
上述脚本是一个简单的完整备份示例，并且假设已经正确配置了 RMAN 环境。我们可以根据实际需求进行修改和定制。

RMAN 将执行脚本中定义的备份操作，并将备份文件保存在指定的目录中。根据我们的配置，备份文件可以是压缩的备份集，并且还可以自动备份控制文件和归档日志。

但是，在实际生产环境中，我们可能还需要考虑其他因素，如备份存储的安全性、备份文件的保留策略、备份的调度和监控等。所以，我们可以在生产环境中与数据库管理员（DBA）或专业人员合作，以确保备份策略符合最佳实践和业务需求。

## 项目总结

本次实验旨在设计一套基于Oracle数据库的商品销售系统，涵盖了数据库设计方案、表空间设计、权限及用户分配方案、存储过程和函数设计以及数据库备份方案。以下是对每个部分的总结：

    数据库设计方案：
    在数据库设计方案中，我们需要考虑到系统的需求和功能，设计合适的表结构来存储商品销售相关的数据。在设计过程中，需要确定合适的字段、数据类型、约束以及表之间的关系，以确保数据的完整性和一致性。

    表及表空间设计方案：
    为了提高数据库的性能和可管理性，我们需要合理地划分表空间。至少创建两个表空间，可以根据需要将不同的表分配到不同的表空间中。此外，我们需要创建至少4张表来存储商品、销售订单、顾客等相关信息，并模拟足够的数据量，以确保系统的可扩展性和性能。

    权限及用户分配方案：
    为了保护数据库的安全性，我们需要设计合理的权限分配方案。至少创建两个用户，分配不同的角色和权限给这些用户，以限制其对数据库对象的访问和操作，同时确保数据的安全性和保密性。

    存储过程和函数设计：
    在数据库中建立一个程序包，并使用PL/SQL语言设计一些存储过程和函数，实现复杂的业务逻辑。这些存储过程和函数可以用于处理商品销售相关的业务需求，如计算订单总金额、获取客户订单数量等。通过存储过程和函数的封装和复用，可以提高数据库的性能和开发效率。

    数据库备份方案：
    设计一套完善的数据库备份方案是确保数据安全和可恢复性的重要措施。备份可以通过使用 Oracle 提供的工具和技术，如 RMAN（Recovery Manager）来实现。合理的备份策略包括定期完整备份和增量备份，以及备份数据的存储位置和保留期限的规划。

综上所述，本次实验通过设计基于Oracle数据库的商品销售系统，涵盖了数据库设计、表空间设计、权限及用户分配、存储过程和函数设计以及数据库备份方案。通过完成这些设计，我们能够构建一个可靠、高效、安全的商品销售系统，满足业务需求并保护数据的安全性和可恢复性。这些设计和实施的过程也加深了对数据库管理和开发的理解和实践能力。

在实验过程中，可能会遇到一些挑战和问题，例如数据模型设计的复杂性、权限分配的合理性、存储过程和函数的优化等。然而，通过不断学习和实践，我们能够克服这些问题并获得宝贵的经验和知识。

总而言之，本次实验为我们提供了一个全面的数据库设计和管理的实践机会，使我们能够应用所学的知识和技能来构建实际的商品销售系统。这对于我们的专业发展和未来的工作具有重要的意义，并且为我们在数据库领域的学习和探索打下了坚实的基础。



