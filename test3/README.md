# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
![](1.png)
![](2.png)
![](3.png)

以上是一个创建表 orders 的 SQL 语句，表 orders 包含以下列：

    order_id：订单号，数字类型，长度为 9 位，不能为空。
    customer_name：顾客姓名，字符串类型，长度为 40 个字节，不能为空。
    customer_tel：顾客电话，字符串类型，长度为 40 个字节，不能为空。
    order_date：订单日期，日期类型，不能为空。
    employee_id：员工号，数字类型，长度为 6 位，不能为空。
    discount：折扣，数字类型，长度为 8 位，保留 2 位小数，缺省值为 0。
    trade_receivable：应收账款，数字类型，长度为 8 位，保留 2 位小数，缺省值为 0。

表 orders 使用了分区技术，按照订单日期进行了分区，分成了 3 个分区：

    PARTITION_BEFORE_2016：订单日期在 2016 年之前的数据。
    PARTITION_BEFORE_2020：订单日期在 2016 年到 2019 年之间的数据。
    PARTITION_BEFORE_2021：订单日期在 2020 年之前的数据。

并预留了一个分区 PARTITION_BEFORE_2022，以后可用于存储订单日期在 2021 年之后的数据。该表所使用的表空间为 USERS，设置了表空间的 PCTFREE 为 10，INITRANS 为 1，未启用压缩和并行执行。分区使用了 NOLOGGING 选项，不记录日志。每个分区都有自己的存储设置，其中 INITIAL 为 8MB，NEXT 为 1MB，MINEXTENTS 为 1，MAXEXTENTS 为无限制，BUFFER_POOL 为默认值。在分区 PARTITION_BEFORE_2016 上设置了 NO INMEMORY，其他分区未设置。最后，表 orders 的主键为 ORDER_ID。

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```
![](4.png)

以上是一个创建表 order_details 的 SQL 语句，表 order_details 包含以下列：

    id：订单明细 ID，数字类型，长度为 9 位，不能为空。
    order_id：订单号，数字类型，长度为 10 位，不能为空。
    product_id：产品 ID，字符串类型，长度为 40 个字节，不能为空。
    product_num：产品数量，数字类型，长度为 8 位，保留 2 位小数，不能为空。
    product_price：产品单价，数字类型，长度为 8 位，保留 2 位小数，不能为空。

1. 表 order_details 也使用了分区技术，使用了 REFERENCE 分区方式，即按照外键关联的 orders 表进行分区。该表所使用的表空间为 USERS，设置了表空间的 PCTFREE 为 10，INITRANS 为 1，未启用压缩和并行执行。每个分区都有自己的存储设置，其中 BUFFER_POOL 为默认值。
2. 表 order_details 的主键为 id，使用了名为 ORDER_DETAILS_PK 的约束。
3. 表 order_details 还使用了名为 order_details_fk1 的外键约束，将订单明细表和订单表关联起来，保证了订单明细表中的 order_id 值必须在订单表中存在。该约束使用了 ENABLE 选项，表示启用该约束。
4. 综上，表 order_details 与表 orders 存在关联关系，使用了 REFERENCE 分区方式进行分区，可以保证在查询时可以提高查询效率。

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
![](5.png)

以上是一个创建序列 SEQ1 的 SQL 语句，该序列的属性如下：

    MINVALUE 1：序列的最小值为 1。
    MAXVALUE 999999999：序列的最大值为 999999999。
    INCREMENT BY 1：序列每次增加的值为 1。
    START WITH 1：序列的起始值为 1。
    CACHE 20：缓存 20 个序列值，可以提高性能。
    NOORDER：不保证序列生成的顺序。
    NOCYCLE：序列生成的值不会循环。
    NOKEEP：当表被删除时，不保留序列。
    NOSCALE：序列值不会自动缩放。
    GLOBAL：序列为全局可见，可以跨多个会话使用。

综上，该序列可以用于自动生成唯一的、递增的整数值，可以被多个会话共享，并且不会循环生成相同的值。在创建表时，可以使用该序列为表的主键字段提供唯一的标识符。

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```
![](6.png)
上述代码是一个PL/SQL代码块，它使用一个循环来插入100个订单记录到数据库表中，每个订单记录包含订单号和日期。以下是这段代码的分析：

    第1行到第4行定义了4个整型变量i、y、m和d，以及一个字符串变量str，这些变量将在后续的代码中使用。
    第6行将变量i初始化为0，变量y初始化为2015，变量m初始化为1，变量d初始化为12。这些变量用于生成日期字符串。
    第7行开始一个while循环，循环次数为100次，即插入100个订单记录。
    第8行将变量i加1，每次循环增加1。
    第10行到第12行修改变量m的值，使其在1到12之间循环变化。当变量m大于12时，将其重置为1。
    第13行将y、m、d三个变量拼接成一个字符串，格式为yyyy-MM-dd，并将其赋值给变量str。
    第14行使用PL/SQL中的insert语句将订单记录插入到orders表中。order_id的值由SEQ1序列生成，order_date的值通过将str转换为日期格式得到。
    第17行使用commit语句提交所有插入操作的结果。
    最后一行结束代码块。

综上所述，这段代码的作用是生成100个订单记录，每个订单记录包含订单号和日期，日期从2015年1月12日开始，每个订单日期增加一个月，直到最后一个订单日期为2015年4月12日。

- 在order_details表中插入数据

```sql
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```
![](7.png)
这段代码是一个PL/SQL代码块，它使用两个循环来插入订单明细数据到数据库表中。以下是这段代码的分析：

    第1行到第3行定义了2个整型变量i和j，以及一个max_order_id变量，用于查询最大的订单号。
    第5行使用SELECT语句查询orders表中最大的订单号，并将其赋值给max_order_id变量。
    第6行将i初始化为max_order_id - 99，即从max_order_id往前推100个订单开始插入订单明细记录。
    第7行开始一个while循环，循环次数为100次，即插入100个订单的订单明细记录。
    第8行开始一个for循环，循环次数为5次，即为每个订单插入5条订单明细记录。
    第9行到第11行使用INSERT语句将订单明细记录插入到order_details表中。每条订单明细记录包括id、order_id、product_id、product_num和product_price字段，其中id由SEQ1序列生成，order_id为当前订单号i，product_id为固定的字符串'product_'加上j的值，product_num为j的值，product_price为j*100的值。
    第12行将i加1，即为下一个订单插入订单明细记录。
    第13行结束while循环。
    第14行使用commit语句提交所有插入操作的结果。
    最后一行结束代码块。

综上所述，这段代码的作用是查询最大的订单号，从该订单号往前推100个订单开始，为每个订单插入5条订单明细记录，每条订单明细记录包括id、order_id、product_id、product_num和product_price字段。其中，订单号和订单明细id由SEQ1序列生成，product_id为固定的字符串'product_'加上j的值，product_num为j的值，product_price为j*100的值。

- 联合查询与执行计划分析
> 查询order_id 范围在200~300内的物品的product_id，product_num，product_price，order_date

```sql
EXPLAIN PLAN FOR
SELECT od.product_id, od.product_num, od.product_price, o.order_date
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
WHERE o.order_id >= 200 AND o.order_id <= 300;

SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
```
![](8.png)
![](9.png)


上述执行计划中，我们可以看到查询语句的具体执行过程，以及每个操作的成本和执行时间。例如，第一行的 SELECT STATEMENT 表示整个查询语句的执行过程，COST 列表示查询的成本，TIME 列表示查询所需的时间。第二行和第三行的 TABLE ACCESS FULL 表示对 orders 表和 order_details 表的全表扫描操作。在实际情况中，执行计划的具体输出可能会因数据库版本、配置和数据分布等因素而有所不同。

## 实验总结
本次实验主要是学习分区表的创建方法及不同分区方式的使用场景，以下是实验的总结：

1. 分区表的创建方法：使用CREATE TABLE语句创建分区表，其中需要指定分区方式、分区键等参数。Oracle提供了多种分区方式，如范围分区、哈希分区、列表分区、复合分区等。

    不同分区方式的使用场景：
    范围分区：适合按照时间、日期等范围进行分区，常用于历史数据的存储。
    哈希分区：适合数据分布均匀的情况下，可以避免数据热点问题。
    列表分区：适合根据某些离散值进行分区，如地域、产品类型等。
    复合分区：可以结合多个分区方式，实现更灵活的分区策略。

2. 实验过程中需要注意的点：

    主外键关系：在创建表时需要考虑表与表之间的关系，并建立主外键关联，保证数据的完整性。
    索引的使用：可以使用B-Tree索引提高查询效率。
    序列的使用：可以使用序列自动生成主键，减少手工操作。
    数据分布的均匀性：为了保证数据能够平均分布到各个分区，可以使用HASH或者ROUND-ROBIN分区方式。

3. 执行计划的分析：在进行联合查询时，需要分析执行计划，以确定查询的效率。可以使用EXPLAIN PLAN语句来获取执行计划，并根据计划进行调优。

4. 分区与不分区的对比实验：实验中可以进行分区与不分区的对比实验，以比较不同分区方式对查询效率的影响，进一步优化数据库的性能。

总之，本次实验学习了分区表的创建方法及不同分区方式的使用场景，并掌握了索引、序列、主外键关联等数据库操作技巧。同时，还学习了执行计划的分析方法，以及分区与不分区的对比实验，进一步提高了数据库设计和性能优化的能力。

